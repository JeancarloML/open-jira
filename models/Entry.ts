import { Schema, model, Model, Document } from "mongoose";
import { Entry } from "../interfaces/entry";
import mongoose from "mongoose";

// An Interface that describes the properties
// that are required to create a new User

export interface EntryAttrs extends Entry {}
type EntryStatus = "pending" | "in-progress" | "finished";

// An Interface that describes the properties
// that a User Model has
interface UserModel extends Model<EntryDoc> {
  build(attrs: EntryAttrs): EntryDoc;
}

// An Interface that describes the properties
// that a User Document has
export interface EntryDoc extends Document {
  description: string;
  createdAt: number;
  status: EntryStatus;
}

const entrySchema = new Schema({
  description: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Number,
  },
  status: {
    type: String,
    enum: {
      values: ["pending", "in-progress", "finished"],
      message: "{VALUE} no es un estado permitido",
    },
    default: "pending",
  },
});

entrySchema.methods.toJSON = function () {
  const entry = this.toObject();
  const { __v /* , _id */, ...newEntry } = entry;
  // newEntry.id = _id;
  return newEntry;
};

entrySchema.statics.build = (attrs: EntryAttrs) => {
  return new EntryModel(attrs);
};

const EntryModel =
  mongoose.models.Entry ||
  mongoose.model<EntryDoc, UserModel>("Entry", entrySchema);

export default EntryModel;
