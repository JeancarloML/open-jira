# Open Jira

* Para instalar las dependencias de la aplicación y correr, ejecutar el siguiente comando:

```bash
yarn install
yarn dev
```

* Para correar localmente, se necesita la base de datos

```docker
docker-compose up -d
```

* El -d, significo __detached__

* MongoDB URL local:

```docker
mongodb://localhost:27017/entriesdb
```

## Configurar las variables de entorno

Renombrar el archivo __.env.templato__ a __.env__

Para ejecutar el proyecto, ejecutar el siguiente comando:

```bash
yarn dev
```

## Llenar la base de datos con información de pruebas

llamar:

```bash
http://localhost:3000/api/seed
```
