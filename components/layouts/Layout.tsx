import Head from "next/head";
import { Box } from "@mui/material";
import { FC } from "react";
import { NavBar, Sidebar } from "../ui";

interface Props {
  title?: string;
}

export const Layout: FC<Props> = ({ title = "OpenJira", children }) => {
  return (
    <Box sx={{ flexFlow: 1, minHeight: "100vh" }}>
      <Head>
        <title>{title}</title>
      </Head>
      <NavBar />
      <Sidebar />
      <Box sx={{ padding: "10px 20px"}}>{children}</Box>
    </Box>
  );
};
