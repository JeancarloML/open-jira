import { Button, Box, TextField } from "@mui/material";
import SaveOutlinedIcon from "@mui/icons-material/SaveOutlined";
import AddCircleOutlineOutlinedIcon from "@mui/icons-material/AddCircleOutlineOutlined";
import { useState, ChangeEvent, useContext } from "react";
import { EntriesContext } from "../../context/entries";
import { UIContext } from "../../context/ui";

export const NewEntry = () => {
  const [inputValue, setInputValue] = useState("");
  const { addNewEntry } = useContext(EntriesContext);
  const { setIsAddingEntry, isAddingEntry } = useContext(UIContext);

  const [touched, setTouched] = useState(false);

  const onTextFieldChange = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setInputValue(event.target.value);
  };

  const onSave = () => {
    if (inputValue.length === 0) return;
    setInputValue("");
    setTouched(false);
    setIsAddingEntry(false);
    addNewEntry(inputValue);
  };

  return (
    <Box sx={{ marginBottom: 2, paddingX: 1 }}>
      {isAddingEntry ? (
        <>
          <TextField
            fullWidth
            sx={{ marginBottom: 1, marginTop: 2 }}
            placeholder="Nueva entrada"
            autoFocus
            multiline
            label="Nueva Entrada"
            helperText={
              inputValue.length === 0 && touched && "Campo obligatorio"
            }
            error={inputValue.length === 0 && touched}
            value={inputValue}
            onChange={onTextFieldChange}
            onBlur={() => setTouched(true)}
          />
          <Box display="flex" justifyContent="space-between">
            <Button
              variant="outlined"
              color="error"
              onClick={() => setIsAddingEntry(false)}
              endIcon={<SaveOutlinedIcon />}
            >
              Cancelar
            </Button>
            <Button
              variant="outlined"
              color="secondary"
              onClick={onSave}
              endIcon={<SaveOutlinedIcon />}
            >
              Guardar
            </Button>
          </Box>
        </>
      ) : (
        <Button
          startIcon={<AddCircleOutlineOutlinedIcon />}
          variant="outlined"
          fullWidth
          onClick={() => setIsAddingEntry(true)}
        >
          Agregar
        </Button>
      )}
    </Box>
  );
};
