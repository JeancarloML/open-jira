interface SeedData {
  entries: SeedEntry[];
}

interface SeedEntry {
  description: string;
  createdAt: number;
  status: string;
}

export const seedData:SeedData = {
  entries: [
    {
      description:
        "Pendiente: Voluptate eu laborum cupidatat laboris sunt consectetur minim occaecat dolor qui non do fugiat anim",
      status: "pending",
      createdAt: Date.now(),
    },
    {
      description:
        "EnProgreso: Voluptate eu laborum cupidatat laboris sunt consectetur minim occaecat dolor qui non do fugiat anim",
      status: "in-progress",
      createdAt: Date.now() - 100000,
    },
    {
      description:
        "Terminadas: Voluptate eu laborum cupidatat laboris sunt consectetur minim occaecat dolor qui non do fugiat anim",
      status: "finished",
      createdAt: Date.now() - 100000,
    },
  ],
};
