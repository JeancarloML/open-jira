import type { NextApiRequest, NextApiResponse } from "next";
import { db } from "../../../database";
import { Entry } from "../../../models";
import { EntryDoc } from "../../../models/Entry";

type Data =
  | {
      message: string;
    }
  | EntryDoc[]
  | EntryDoc;

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  switch (req.method) {
    case "GET":
      return getEntries(res);
    case "POST":
      return postEntry(req, res);
    default:
      return res.status(405).json({
        message: "Method not allowed",
      });
  }
}

const getEntries = async (res: NextApiResponse<Data>) => {
  await db.connect();
  const entries = await Entry.find().sort({ createdAt: "ascending" });
  await db.disconnect();

  res.status(200).json(entries);
};

const postEntry = async (req: NextApiRequest, res: NextApiResponse<Data>) => {
  try {
    /*const { description } = JSON.parse(req.body);*/
    const { description } = req.body;
    const entry = new Entry({
      description,
      createdAt: Date.now(),
    });
    await db.connect();
    await entry.save();
    await db.disconnect();

    res.status(200).json(entry);
  } catch (error) {
    await db.disconnect();
    console.log(error);
    res.status(500).json({
      message: "Error al crear la entrada",
    });
  }
};
