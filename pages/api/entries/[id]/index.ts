import type { NextApiRequest, NextApiResponse } from "next";
import { db } from "../../../../database";
import { Entry } from "../../../../models";
import { EntryDoc } from "../../../../models/Entry";
// import mongoose from "mongoose";

type Data =
  | {
      message: string;
    }
  | EntryDoc[]
  | EntryDoc;

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  /*  const { id } = req.query;
  if (!mongoose.isValidObjectId(id)) {
    return res.status(400).json({
      message: "Invalid id",
    });
  } */
  switch (req.method) {
    case "PATCH":
      return patchEntry(req, res);
    case "GET":
      return getEntry(req, res);
    default:
      return res.status(405).json({
        message: "Method not allowed",
      });
  }
}

const patchEntry = async (req: NextApiRequest, res: NextApiResponse<Data>) => {
  try {
    const { id } = req.query;
    const { description, status } = req.body;
    await db.connect();
    const updatedEntry = await Entry.findByIdAndUpdate(
      id,
      { status, description },
      {
        new: true,
        runValidators: true,
      }
    );
    await db.disconnect();
    res.status(200).json(updatedEntry);
  } catch (error: any) {
    await db.disconnect();
    res.status(500).json({
      message: error.errros.status.message,
    });
  }
};

const getEntry = async (req: NextApiRequest, res: NextApiResponse<Data>) => {
  try {
    const { id } = req.query;
    await db.connect();
    const entry = await Entry.findById(id);
    if (!entry) {
      return res.status(404).json({
        message: "Entry not found",
      });
    }
    await db.disconnect();
    res.status(200).json(entry);
  } catch (error: any) {
    await db.disconnect();
    res.status(500).json({
      message: error.errros.status.message,
    });
  }
};
