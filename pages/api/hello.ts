// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { db } from "../../database";

type Data = {
  name: string;
  method: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  if (process.env.NODE_ENV === "production") {
    return res.status(401).json({ name: "Example", method: "production" });
  }

  await db.connect();

  await db.disconnect();
  res
    .status(200)
    .json({ name: "John Doe", method: req.method || "No hay metodo" });
}
